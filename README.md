# Exercice de diagramme de temps


Nous aimerions réaliser un diagramme de temps pour un site internet afin d'évaluer le temps qu'un visiteur devrait attendre avant de visualiser un rendu au sein de son navigateur. 

Lorsque le visiteur saisit une URL au sein de son navigateur, l'URL devrait être résolue en une adresse IP. 
La résolution DNS peut ajouter un certain temps de latence perceptible par le visiteur. 

Le temps de latence lié à la résolution DNS peut être d'une milli seconde (DNS local) à quelques secondes. 

Avec une implémentation classique MVC (Modèle Vue Controlleur), une servlet java récupère le contrôle de l'appel et requête la base de données afin d'accéder à des modèles. 

La communication avec une base de données a tendance à être impactante en allocation de temps. Après avoir reçu et traité laa donnée, la servlet transfère les données à la vue. 

La réponse HTTP est ensuite mise en buffer et envoyée vers le navigateur du visiteur. 

Le navigateur prend un peu de temps également pour traiter la réponse HTTP et commence à réaliser un rendu HTML au sein du client web. 
A noter que celà peut mener à certaines verifications ou nouveaux appels comme le téléchargement du fichier CSS, JS ou des images.  Ces nouveaux appels et vérifications ne seront pas à représenter au sein de ce diagramme. 